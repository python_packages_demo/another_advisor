from ideas_provider import suggest_company
from hashlib import md5
from datetime import datetime


def give_alternative_advice() -> dict:
    company = suggest_company()
    name = company[1]
    code = company[0]
    dt: str = datetime.utcnow().date().isoformat()
    buy: bool = md5(f'{name}@{dt}'.encode('utf-8')).digest()[-1] % 2 == 0
    return {'code': code,
            'recommendation': 'buy' if buy else 'sell',
            'name': name
            }


if __name__ == '__main__':
    advice = give_alternative_advice()
    print(f'{advice["recommendation"]} some {advice["code"]} stocks today')
